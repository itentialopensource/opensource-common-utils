# Documentation Pipeline Tool

A Documentation tool that automatically generates TAB1 and TAB2 based on other markdown files. Automatically runs through the adapter and pre-built pipelines.

## File Usage

documentation-pipeline-tool.js is the tool ran in the pipeline for auto generation for TAB1 and TAB2

commit.sh is the script used for auto commiting the generated files

projectTypeTemplates folder has generic templates to be used for generation of TAB1 and TAB2