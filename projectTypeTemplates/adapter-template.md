## Table of Contents

- [Getting Started](#getting-started)
  - [Helpful Background Information](#helpful-background-information)
  - [Prerequisites](#prerequisites)
  - [How to Install](#how-to-install)
  - [Testing](#testing)
- [Configuration](#configuration)
  - [Example Properties](#example-properties)
  - [Connection Properties](#connection-properties)
  - [Authentication Properties](#authentication-properties)
  - [Healthcheck Properties](#healthcheck-properties)
  - [Request Properties](#request-properties)
  - [SSL Properties](#ssl-properties)
  - [Throttle Properties](#throttle-properties)
  - [Proxy Properties](#proxy-properties)
  - [Mongo Properties](#mongo-properties)
  - [Device Broker Properties](#device-broker-properties)
  - [Cache Properties](#cache-properties)
- [Using this Adapter](#using-this-adapter)
  - [Generic Adapter Calls](#generic-adapter-calls)
  - [Adapter Cache Calls](#adapter-cache-calls)
  - [Adapter Broker Calls](#adapter-broker-calls)
  - [Specific Adapter Calls](#specific-adapter-calls)
  - [Authentication](#authentication)
- [Additional Information](#additional-information)
  - [Enhancements](#enhancements)
  - [Contributing](#contributing)
  - [Helpful Links](#helpful-links)
  - [Node Scripts](#node-scripts)
- [Troubleshoot](#troubleshoot)
  - [Connectivity Issues](#connectivity-issues)
  - [Functional Issues](#functional-issues)

## End of Table of Contents

### Helpful Links

<a href="https://docs.itential.com/opensource/docs/adapters" target="_blank">Adapter Technical Resources</a>

### Node Scripts

There are several node scripts that now accompany the adapter. These scripts are provided to make several activities easier. Many of these scripts can have issues with different versions of IAP as they have dependencies on IAP and Mongo. If you have issues with the scripts please report them to the Itential Adapter Team. Each of these scripts are described below.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Run</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:install</td>
    <td style="padding:15px">Provides an easier way to install the adapter.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:checkMigrate</td>
    <td style="padding:15px">Checks whether your adapter can and should be migrated to the latest foundation.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:findPath</td>
    <td style="padding:15px">Can be used to see if the adapter supports a particular API call.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:migrate</td>
    <td style="padding:15px">Provides an easier way to update your adapter after you download the migration zip from Itential DevSite.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:update</td>
    <td style="padding:15px">Provides an easier way to update your adapter after you download the update zip from Itential DevSite.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run adapter:revert</td>
    <td style="padding:15px">Allows you to revert after a migration or update if it resulted in issues.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run troubleshoot</td>
    <td style="padding:15px">Provides a way to troubleshoot the adapter - runs connectivity, healthcheck and basic get.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run connectivity</td>
    <td style="padding:15px">Provides a connectivity check to the Servicenow system.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run healthcheck</td>
    <td style="padding:15px">Checks whether the configured healthcheck call works to Servicenow.</td>
  </tr>
  <tr>
    <td style="padding:15px">npm run basicget</td>
    <td style="padding:15px">Checks whether the basic get calls works to Servicenow.</td>
  </tr>
</table>
<br>
