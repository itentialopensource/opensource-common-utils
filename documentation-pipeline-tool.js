#!/usr/bin/env node

const _yargs = require("yargs");
const fs = require("fs");
const path = require("path");

main()
  .then()
  .catch((reason) => console.log(reason));
async function main() {
  try {
    const yargs = _yargs(process.argv.slice(2));
    const argv = await yargs
      .option("projectPath", { type: "string", require: true })
      .option("projectUrl", { type: "string", require: true })
      .alias("projectPath", "p")
      .alias("projectUrl", "u").argv;

    let projectPath = argv.projectPath;
    let metadataPath = `${projectPath}/metadata.json`;
    let projectType = await checkType(metadataPath);
    let projectName = await checkName(metadataPath);
    let projectUrl = argv.projectUrl;
    if (projectType === "Adapter") {
      generateAdapterTab1(projectPath);
      generateAdapterTab2(projectPath, projectUrl);
    } else {
      generatePrebuiltTab1(projectName, projectType, projectPath);
      generatePrebuiltTab2(projectName, projectType, projectPath);
    }
  } catch (error) {
    console.error(`Error in main: ${error}`);
  }
}

function extractSectionFromMarkdown(
  filePath,
  startMarker,
  outputFilePath,
  titleFlag = true
) {
  try {
    let capturedText = "";
    const markdown = fs.readFileSync(filePath, "utf8");
    const lines = markdown.split("\n");
    let capturing = false;
    const startMarkerLevel = startMarker.trim().split(" ")[0].length;
    for (const line of lines) {
      if (line.startsWith(startMarker)) {
        capturing = true;
        if (titleFlag) {
          capturedText += line + "\n";
        }
        continue;
      }

      const lineMarkerMatch = line.match(/^(#{1,6})\s/);
      if (capturing && lineMarkerMatch) {
        const lineMarkerLevel = lineMarkerMatch[1].length;
        if (lineMarkerLevel <= startMarkerLevel) {
          break;
        }
      }

      if (capturing) {
        capturedText += line + "\n";
      }
    }
    fs.appendFileSync(outputFilePath, capturedText);
    return capturedText.trim();
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return "";
  }
}

function extractParagraph(
  filePath,
  startMarker,
  paragraphNumber,
  outputFilePath
) {
  try {
    let capturedText = "";
    const markdown = fs.readFileSync(filePath, "utf8");
    const lines = markdown.split("\n");
    let capturing = false;
    const startMarkerLevel = startMarker.trim().split(" ")[0].length;
    let paragraphCount = 0;

    for (const line of lines) {
      if (line.startsWith(startMarker)) {
        capturing = true;
        continue;
      }

      const lineMarkerMatch = line.match(/^(#{1,6})\s/);
      if (capturing && lineMarkerMatch) {
        const lineMarkerLevel = lineMarkerMatch[1].length;
        if (lineMarkerLevel <= startMarkerLevel) {
          break;
        }
      }
      if (capturing) {
        paragraphCount++;
        capturedText += line + "\n";
      }
      if (paragraphCount > paragraphNumber) {
        break;
      }
    }
    fs.appendFileSync(outputFilePath, capturedText + "\n");
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return "";
  }
}

function extractSectionWithSmallerTitle(
  filePath,
  startMarker,
  outputFilePath,
  titleFlag = true,
  numOfLevelsDown = 1
) {
  try {
    let capturedText = "";
    const markdown = fs.readFileSync(filePath, "utf8");
    const lines = markdown.split("\n");
    let capturing = false;
    const startMarkerLevel = startMarker.trim().split(" ")[0].length;
    for (let line of lines) {
      if (line.startsWith(startMarker)) {
        capturing = true;
        if (titleFlag) {
          capturedText += "#".repeat(numOfLevelsDown) + line + "\n";
        }
        continue;
      }

      const lineMarkerMatch = line.match(/^(#{1,6})\s/);
      if (capturing && lineMarkerMatch) {
        line = "#".repeat(numOfLevelsDown) + line;
        const lineMarkerLevel = lineMarkerMatch[1].length;
        if (lineMarkerLevel <= startMarkerLevel) {
          break;
        }
      }

      if (capturing) {
        capturedText += line + "\n";
      }
    }
    fs.appendFileSync(outputFilePath, capturedText);
    return capturedText.trim();
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return "";
  }
}

function extractFileAndModifyContent(
  inputFile,
  replaceStartMarker,
  replaceEndMarker,
  whichOccurenceToReplace,
  contentToReplace
) {
  try {
    const markdown = fs.readFileSync(inputFile, "utf8");
    const lines = markdown.split("\n");
    let capturing = false;
    let occurence = 0;
    let capturedText = "";
    for (let line of lines) {
      if (line.startsWith(replaceStartMarker)) {
        occurence++;
        if (occurence === whichOccurenceToReplace) {
          capturing = true;
          continue;
        }
      }

      if (!capturing) {
        capturedText += line + "\n";
      }

      if (capturing && line.startsWith(replaceEndMarker)) {
        capturedText += contentToReplace + "\n";
        capturing = false;
      }
    }
    return capturedText;
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return "";
  }
}

function checkType(filePath) {
  try {
    let data = JSON.parse(fs.readFileSync(filePath, "utf8"));
    return data.type;
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    throw error;
  }
}

function checkName(filePath) {
  try {
    let data = JSON.parse(fs.readFileSync(filePath, "utf8"));
    return data.name;
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    throw error;
  }
}

function checkIAPVersion(filePath) {
  try {
    let data = JSON.parse(fs.readFileSync(filePath, "utf8"));
    return data.iapVersions.pop();
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    throw error;
  }
}

function generatePrebuiltNameHeader(projectType, projectPath) {
  try {
    let filePath = `${projectPath}/metadata.json`;
    let data = JSON.parse(fs.readFileSync(filePath, "utf8"));

    return `# ${data.name.replace(/ - /g, " ")} ${projectType}\n`;
  } catch (error) {
    throw error;
  }
}

function generateAdapterNameHeader(projectPath) {
  try {
    let filePath = `${projectPath}/SYSTEMINFO.md`;
    const markdown = fs.readFileSync(filePath, "utf8");
    const lines = markdown.split("\n");
    return `${lines[0]}\n`;
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return "";
  }
}

function generateTableOfContents(filePath) {
  try {
    if (filePath === undefined) {
      throw Error("File path is required");
    }
    let file = fs.readFileSync(filePath, "utf8");
    let lines = file.split("\n");
    let tableOfContents = "";
    for (let line of lines) {
      if (line.startsWith("#")) {
        let level = line.match(/#/g).length;
        if (level <= 1 || level >= 4) continue;
        if (line.includes("Table of Contents")) continue;
        let title = line.replace(/#/g, "").trim();
        // Check if title has <ins> and </ins> wrapped, if so, remove these tags
        if (/<ins>.*<\/ins>/.test(title)) title = title.replace(/<ins>|<\/ins>/g, "");
        tableOfContents += `${"  ".repeat(level - 1)}- [${title}](#${title
          .toLowerCase()
          .replace(/ - | /g, "-")})\n`;
      }
    }
    return tableOfContents;
  } catch (error) {
    console.error(`Error generating table of contents: ${error}`);
    return "";
  }
}

function checkSectionExists(filePath, section) {
  try {
    const content = fs.readFileSync(filePath, "utf8");
    return content.includes(section);
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return false;
  }
}

function extractTwoSections(filePath, outputFilePath) {
  try {
    const content = fs.readFileSync(filePath, "utf8");
    const lines = content.split("\n");
    let capturing = false;
    let sectionCount = 0; // To count the '##' sections captured
    let capturedText = "";

    for (const line of lines) {
      // Detect any '##' heading
      if (line.startsWith("##")) {
        // Increment sectionCount each time a '##' is encountered
        sectionCount++;

        // Start capturing from the first '##' heading
        if (sectionCount === 1) {
          capturing = true;
        }
        // Stop capturing after the second '##' section has been captured
        else if (sectionCount > 2) {
          break;
        }
      }

      // Capture the line if we are within the first two '##' sections
      if (capturing && sectionCount <= 2 && line !== "## Overview") {
        capturedText += line + "\n";
      }
    }

    // Save the captured text to the specified output file
    fs.appendFileSync(outputFilePath, capturedText.trim() + "\n");
    console.log(`Extracted content successfully saved to ${outputFilePath}`);
  } catch (error) {
    console.error(`Error processing file: ${error}`);
  }
}

function generatePrebuiltTab1(projectName, projectType, projectPath) {
  try {
    if (projectName === undefined || projectType === undefined) {
      throw Error("Project name and type are required");
    }

    let outputFile = `${projectPath}/TAB1.md`;
    let dynamicSection = `\nFor further technical details on how to install and use this ${projectType}, please click the Technical Documentation tab. \n`;
    let inputFile = `${projectPath}/README.md`;

    fs.writeFileSync(`${outputFile}`, "# Overview \n\n");
    extractTwoSections(inputFile, outputFile);
    fs.appendFileSync(`${outputFile}`, dynamicSection);

    const data = fs.readFileSync(outputFile, "utf8");
    const hyperlinkRegex =
      /<a href=['"][^'"]*['"](?: target=['"]_blank['"])?>(.*?)<\/a>/g;
    const cleanedData = data.replace(hyperlinkRegex, "$1");
    fs.writeFileSync(outputFile, cleanedData, "utf8");

    if (fs.existsSync(`${projectPath}/TAB1.md`)) {
      console.log(`TAB1.md for prebuilt generated successfully`);
    } else {
      console.error(`TAB1.md for prebuilt not generated`);
    }
  } catch (error) {
    console.error(`Error generating prebuilt tab1: ${error}`);
    throw error;
  }
}

function generatePrebuiltTab2(projectName, projectType, projectPath) {
  try {
    if (projectName === undefined || projectType === undefined) {
      throw Error("Project name and type are required");
    }
    let outputFile = `${projectPath}/TAB2.md`;
    let documentationPath = `${projectPath}/documentation`;
    let projectTemplatePath = `./tabUtils`;

    // fs.appendFileSync(outputFile, `## Table Of Contents\n`);
    // extractSectionFromMarkdown(
    //   `${projectTemplatePath}/prebuilt-table-of-contents.md`,
    //   `## Table Of Contents - ${projectType}`,
    //   outputFile,
    //   false
    // );
    fs.writeFileSync(outputFile, `** ToC start\n`);
    fs.appendFileSync(outputFile, `** ToC end\n`);

    fs.appendFileSync(outputFile, `## Getting Started\n`);
    extractSectionFromMarkdown(
      `${projectTemplatePath}/project-templates.md`,
      `## Getting Started - ${projectType}`,
      outputFile,
      false
    );

    fs.appendFileSync(outputFile, `### Prerequisites\n`);
    let documentation = [];
    documentation = fs.readdirSync(documentationPath);
    let docFiltered = [];
    documentation.forEach((file) => {
      if (path.extname(file) == ".md") {
        docFiltered.push(file);
      }
    });

    let obj = {};
    obj["_VERSION"] = checkIAPVersion(`${projectPath}/metadata.json`);
    obj["_REPLACE"] = projectName;
    replaceMultiple(outputFile, obj);

    extractSectionFromMarkdown(
      `${projectTemplatePath}/project-templates.md`,
      `## Itential Dependencies - ${projectType}`,
      outputFile,
      false
    );

    extractSectionWithSmallerTitle(
      `./README.md`,
      `## External Dependencies`,
      outputFile,
      true,
      1
    );

    extractSectionWithSmallerTitle(
      `./README.md`,
      `## Adapters`,
      outputFile,
      true,
      1
    );

    fs.appendFileSync(outputFile, "\n### How to Install\n");

    extractSectionFromMarkdown(
      `${projectTemplatePath}/project-templates.md`,
      `## How To Install - ${projectType}`,
      outputFile,
      false
    );

    // fs.appendFileSync(outputFile, "### Configuration\n");
    // extractSectionFromMarkdown(
    //   `${projectTemplatePath}/project-templates.md`,
    //   `## Configuration - ${projectType}`,
    //   outputFile,
    //   false
    // );

    extractSectionFromMarkdown(
      `${projectTemplatePath}/project-templates.md`,
      `## Using this ${projectType}`,
      outputFile,
      true
    );

    docFiltered.forEach((file) => {
      let fileName = file.split(".")[0];
      fs.appendFileSync(outputFile, `### <ins>${fileName}</ins>`);
      extractSectionWithSmallerTitle(
        `${documentationPath}/${fileName}.md`,
        `## Overview`,
        outputFile,
        false,
        1
      );
      extractSectionWithSmallerTitle(
        `${documentationPath}/${fileName}.md`,
        `## Using this ${projectType}`,
        outputFile,
        false,
        1
      );

      fs.appendFileSync(outputFile, `\n---\n`);
    });

    extractSectionFromMarkdown(
      `${projectTemplatePath}/project-templates.md`,
      `## Additional Information`,
      outputFile,
      true
    );
    const tableOfContents =
      "## Table of Contents \n" + "\n" + generateTableOfContents(outputFile);
    fs.writeFileSync(
      outputFile,
      extractFileAndModifyContent(
        outputFile,
        "** ToC start",
        "** ToC end",
        1,
        tableOfContents
      )
    );
    if (fs.existsSync(`${projectPath}/TAB2.md`)) {
      console.log(`TAB2.md for prebuilt generated successfully`);
    } else {
      console.error(`TAB2.md for prebuilt not generated`);
    }
  } catch (error) {
    console.error(`Error generating prebuilt tab2: ${error}`);
    throw error;
  }
}

function generateAdapterTab1(projectPath) {
  try {
    let outputFile = `${projectPath}/TAB1.md`;
    fs.writeFileSync(`${outputFile}`, "# Overview \n");
    let overviewSectionInputFile = `${projectPath}/SUMMARY.md`;
    let overviewSection = "## Overview";
    if (fs.existsSync(overviewSectionInputFile)) {
      extractParagraph(
        overviewSectionInputFile,
        overviewSection,
        1,
        outputFile
      );
    } else {
      fs.writeFileSync(
        `${outputFile}`,
        "Note: The content for this section may be missing as its corresponding .md file is unavailable. This sections will be updated once SUMMARY.md file is added.\n"
      );
    }

    let detailsSectionInputFile = `${projectPath}/SYSTEMINFO.md`;
    let detailsSection = "## Why Integrate";
    if (checkSectionExists(detailsSectionInputFile, detailsSection)) {
      fs.appendFileSync(`${outputFile}`, "## Details \n");
      extractSectionFromMarkdown(
        detailsSectionInputFile,
        detailsSection,
        outputFile,
        false
      );
    }

    let dynamicSection = `For further technical details on how to install and use this adapter, please click the Technical Documentation tab. \n`;
    fs.appendFileSync(`${outputFile}`, dynamicSection);
    if (fs.existsSync(`${projectPath}/TAB1.md`)) {
      console.log(`TAB1.md for adapter generated successfully`);
    } else {
      console.error(`TAB1.md for adapter not generated`);
    }
  } catch (error) {
    console.error(`Error generating adapter tab1: ${error}`);
    throw error;
  }
}

function generateAdapterTab2(projectPath, projectUrl) {
  try {
    let outputFile = `${projectPath}/TAB2.md`;
    fs.writeFileSync(outputFile, generateAdapterNameHeader(projectPath) + "\n");

    // fs.appendFileSync(outputFile, `** ToC start\n`);
    // fs.appendFileSync(outputFile, `** ToC end\n`);
    const tableOfContents =
      "## Table of Contents \n" +
      "\n" +
      "  - [Specific Adapter Information](#specific-adapter-information) \n" +
      "    - [Authentication](#authentication) \n" +
      "    - [Sample Properties](#sample-properties) \n" +
      "    - [Swagger](#swagger) \n" +
      "  - [Generic Adapter Information](#generic-adapter-information) \n";
    fs.appendFileSync(outputFile, tableOfContents + "\n");

    fs.appendFileSync(outputFile, `## Specific Adapter Information\n`);
    let authenticationSubsectionInputFile = `${projectPath}/AUTH.md`;
    fs.appendFileSync(outputFile, `### Authentication\n`);
    if (fs.existsSync(authenticationSubsectionInputFile)) {
      extractSectionWithSmallerTitle(
        authenticationSubsectionInputFile,
        `## Authenticating`,
        outputFile,
        false
      );
    } else {
      fs.appendFileSync(
        outputFile,
        `Note: The content for this section may be missing as its corresponding .md file is unavailable. This sections will be updated once AUTH.md file is added.\n`
      );
    }

    fs.appendFileSync(outputFile, `### Sample Properties\n` + "\n");
    let samplePropertiesInputFile = `${projectPath}/sampleProperties.json`;
    let sampleProperties = fs.readFileSync(samplePropertiesInputFile, "utf8");
    let samplePropertiesJSON = JSON.parse(sampleProperties);
    let examplePropertiesSubsectionContent;
    if (fs.existsSync(samplePropertiesInputFile)) {
      examplePropertiesSubsectionContent =
        "```json\n  " +
        `"properties": ${JSON.stringify(
          samplePropertiesJSON.properties,
          null,
          2
        ).replace(/\n/g, "\n  ")}` +
        "\n" +
        "```";
    } else {
      examplePropertiesSubsectionContent =
        "Note: The content for this section may be missing as its corresponding .json file is unavailable. This sections will be updated once sampleProperties.json file is added.\n";
    }
    let text =
      "Sample Properties can be used to help you configure the adapter in the Itential Automation Platform. You will need to update connectivity information such as the host, port, protocol and credentials.\n";
    fs.appendFileSync(outputFile, text + "\n");
    fs.appendFileSync(outputFile, examplePropertiesSubsectionContent + "\n");

    let atIndex = projectUrl.indexOf("@");
    let newUrl = "https://" + projectUrl.substring(atIndex + 1);
    let cleanedLink = newUrl.replace(/\.git$/, "");
    if (fs.existsSync(`${projectPath}/report/adapter-openapi.json`)) {
      fs.appendFileSync(
        outputFile,
        `### [Swagger](` +
          cleanedLink +
          `/-/blob/master/report/adapter-openapi.json) \n` +
          "\n"
      );
    } else {
      fs.appendFileSync(outputFile, `### Swagger\n` + "\n");
      fs.appendFileSync(
        outputFile,
        `Note: The content for this section may be missing as its corresponding .json file is unavailable. This sections will be updated once adapter-openapi.json file is added.\n`
      );
    }

    fs.appendFileSync(
      outputFile,
      `## [Generic Adapter Information](` +
        cleanedLink +
        `/-/blob/master/README.md) \n` +
        "\n"
    );

    // const tableOfContents =
    //   "## Table of Contents \n" + "\n" + generateTableOfContents(outputFile);
    // fs.writeFileSync(
    //   outputFile,
    //   extractFileAndModifyContent(
    //     outputFile,
    //     "** ToC start",
    //     "** ToC end",
    //     1,
    //     tableOfContents
    //   )
    // );
    if (fs.existsSync(`${projectPath}/TAB2.md`)) {
      console.log(`TAB2.md for adapter generated successfully`);
    } else {
      console.error(`TAB2.md for adapter not generated`);
    }
  } catch (error) {
    console.error(`Error generating adapter tab2: ${error}`);
    throw error;
  }
}

function replaceMultiple(outputFile, replacements) {
  fs.readFile(outputFile, "utf8", function (err, data) {
    if (err) {
      console.log(err);
      return;
    }

    let modifiedData = data;

    Object.keys(replacements).forEach((oldValue) => {
      let regExp = new RegExp(oldValue, "g");
      modifiedData = modifiedData.replace(regExp, replacements[oldValue]);
    });

    fs.writeFile(outputFile, modifiedData, "utf8", function (err) {
      if (err) {
        console.log(err);
      }
    });
  });
}
