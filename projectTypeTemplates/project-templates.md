## Getting Started - Example Project

This section is helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information 

Workflows and processes often include logic that varies from business to business. Our Example Projects are more complex processes that include several of our modular components to build a more complete process.

While these can be utilized, you may find more value in using them as a starting point to build around.

## Getting Started - Transformation Project

This section is helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information

Transformation are used to translate data from one format to a different format.

## Getting Started - Workflow Project

This section is helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Helpful Background Information

Workflows often include logic that varies from business to business. As a result, we often find that our Workflow Projects are more useful as modular components that can be incorporated into a larger process. In addition, they often can add value as a learning tool on how we integrate with other systems and how we do things within the Itential Automation Platform.

While these can be utilized, you may find more value in using them as a starting point to build around.


## Configuration - Example Project

Example Projects do not need configuration. When you run a Workflow, anything that it needs in order to run will be requested through initial data entry or forms during the workflow.

## Configuration - Workflow Project

Example Projects do not need configuration. When you run a Workflow, anything that it needs in order to run will be requested through initial data entry or forms during the workflow.

## Configuration - Transformation Project

Transformation Projects do not need configuration.

## Using this Example Project
Example Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs.

## Using this Transformation Project
Transformation Projects contain 1 or more transformations. Each of these transformations have different inputs and outputs.

## Using this Workflow Project
Workflow Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs. 

## Itential Dependencies - Transformation Project

Itential Transformation Projects are built and tested on particular versions of IAP. In addition, Transformation Projects are often dependent on external systems and as such, these Transformation Projects will have dependencies on these other systems. This version of **_REPLACE** has been tested with:

- IAP **_VERSION**

## Itential Dependencies - Workflow Project

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **_REPLACE** has been tested with:

- IAP **_VERSION**

## Itential Dependencies - Example Project

Itential Example Projects are built and tested on particular versions of IAP. In addition, Example Projects are often dependent on external systems and as such, these Example Projects will have dependencies on these other systems. This version of **_REPLACE** has been tested with:

- IAP **_VERSION**

## How To Install - Workflow Project

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Example Project.
- Import the Example Project in [Admin Essentials](https://docs.itential.com/docs/importing-pre-built-iap).

###  Testing

Cypress is generally used to test all Itential Example Projects. While Cypress is an opensource tool, at Itential we have internal libraries that have been built around Cypress to allow us to test with a deployed IAP.

When certifying our Example Projects for a release of IAP we run these tests against the particular version of IAP and create a release branch in GitLab. If you do not see the Example Project available in your version of IAP please contact Itential.

While Itential tests this Example Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Example Project into a development/testing environment in which you can test the Example Project.

## How To Install - Transformation Project

To install the Transformation Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Transformation Project.
- Import the Transformation Project in [Admin Essentials](https://docs.itential.com/docs/importing-pre-built-iap).

###  Testing

Cypress is generally used to test all Itential Transformation Projects. While Cypress is an opensource tool, at Itential we have internal libraries that have been built around Cypress to allow us to test with a deployed IAP.

When certifying our Transformation Projects for a release of IAP we run these tests against the particular version of IAP and create a release branch in GitLab. If you do not see the Transformation Project available in your version of IAP please contact Itential.

While Itential tests this Transformation Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Transformation Project into a development/testing environment in which you can test the Transformation Project.

## How To Install - Example Project

To install the Example Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Example Project.
- Import the Example Project in [Admin Essentials](https://docs.itential.com/docs/importing-pre-built-iap).

### Testing

Cypress is generally used to test all Itential Example Projects. While Cypress is an opensource tool, at Itential we have internal libraries that have been built around Cypress to allow us to test with a deployed IAP.

When certifying our Example Projects for a release of IAP we run these tests against the particular version of IAP and create a release branch in GitLab. If you do not see the Example Project available in your version of IAP please contact Itential.

While Itential tests this Example Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Example Project into a development/testing environment in which you can test the Example Project.

## Using this Workflow Project

Workflow Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs. 

## Using this Transformation Project

Transformation Projects contain 1 or more transformations. Each of these transformations have different inputs and outputs.

## Using this Example Project

Example Projects contain 1 or more workflows. Each of these workflows have different inputs and outputs.

## Additional Information

### Support
Please use your Itential Customer Success account if you need support when using this Workflow Project.

